
<div class="container mt-sm-5 my-1">
    <h2>Chọn đáp án đúng ứng với câu hỏi</h2>
    <form method = "POST" action="index.php?page=submit">
        <?php 
        $count = 0;
            foreach($question as $key => $value){
                $count++;
                if($count <6) continue;
        ?>
        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 h5"><b><?php echo $value['question']; ?></b></div>
            <div class="ml-md-3 ml-sm-3 pl-md-5 pt-sm-0 pt-3" id="options">
                <?php 
                    foreach ($value['answer'] as $val => $an){
                ?>
                <label class="options"><?php echo $an;?>
                    <input type="radio" value="<?php echo $val;?>" name="<?php echo $key; ?>">
                    <span class="checkmark"></span>
                </label>
                <?php } ?>
            </div>
        </div>
        <?php
        }
        
        ?>

        <div class="d-flex align-items-center pt-3">
            <div style="float:right" class="ml-auto mr-sm-5">
            <input style="background-color:#21bf73" type="submit" value="Nộp bài" name="submit" class="btn btn-primary"/>
            </div>
        </div>
    </form>
</div>
<!--  -->